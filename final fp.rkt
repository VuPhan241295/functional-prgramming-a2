#lang racket


(require srfi/1)
(require srfi/13)

                 ;items that you can get
(define objects '((1 "a bronze coin")
                  (1 "coin bag")
                  (2 "a mint coin")
                  (2 "a silver coin")
                  (6 "a gold coin")
                  (6 "a ruby coin")
                  (10 "a diamond coin")))

 
                    ;locations of people
(define locations '((2 "people from Manchester")
                    (5 "people from Liverpool")
                    (4 "people fromLeeds")
                    (6 "people from Newcastle")
                    (3 "people from Birmingham")
                    (9 "people from Bristol")
                    (10 "people from Bath")))
  
  

                  

                       ;Start of the game 
(define descriptions'  ((1 "You are in London.")
                       (2 "You are in Manchester.")
                       (3 "You are in Birmingham.")
                       (4 "You are in Leeds.")
                       (5 "You are in Liverpool.")
                       (6 "You are in Newcastle.")
                       (7 "You are in Norwich.")
                       (8 "You are in Leicester")
                       (9 "You are in Bristol.")
                       (10 "You are in Bath.")))


;look around the map
(define look '(((directions) look) ((look) look) ((examine room) look)))
;quit the game
(define quit '(((exit game) quit) ((quit game) quit) ((exit) quit) ((quit) quit)))
;pick up items
(define pick '(((get) pick) ((pickup) pick) ((pick) pick)))
;put items in and out
(define put '(((put) drop) ((drop) drop) ((place) drop) ((remove) drop)))
;user inventory
(define inventory '(((inventory) inventory) ((bag) inventory)))
;actions which user can perform
(define actions `(,@look ,@quit ,@pick ,@put ,@inventory))

;user can decide where to go
(define decisiontable `((1 ((north) 2) ((south) 3) ((east) 11 ,@actions))
                        (2 ((south) 1) ((east) 3) ((west) 7 ,@actions))
                        (3 ((north) 6) ((west) 2) ((east) 4 ,@actions))
                        (4 ((west) 3) ,@actions)
                        (5 ((north) 1) ((east) 9) ((south) 8 ,@actions))
                        (6 ((south) 3) ,@actions)
                        (7 ((south) 10) ((east) 2) ,@actions)
                        (8 ((north) 5) ,@actions)
                        (9 ((west) 5) ,@actions)
                        (10 ((north) 7) ((east) 1) ,@actions)))
                        
                        
                        
                        
                        
  

;3 has table defined
(define objectdb (make-hash))
(define inventorydb (make-hash))
(define locationdb (make-hash))


(define (add-object db id object)
  ;check if the has key exists
  (if (hash-has-key? db id)
      (let ((record (hash-ref db id)))
        (hash-set! db id (cons object record)))
      (hash-set! db id (cons object empty))))


(define (add-location db id location)
  ;check if the has key exists
  (if (hash-has-key? db id)
      (let ((record (hash-ref db id)))
        (hash-set! db id (cons location record)))
      (hash-set! db id (cons location empty))))

;for eacdh funtion used
;add my items (obejects) to the object databse
(define (add-objects db)
  (for-each
   (lambda (r) 
     (add-object db (first r) (second r))) objects))


;for each function used
;add my locations to the location database
(define (add-locations db)
  (for-each
   (lambda (r) 
     (add-location db (first r) (second r))) locations))



;;populate objectdb with the items provided in the object
(add-objects objectdb)

;;populate locationdb with the items provided in location
(add-locations locationdb)



;display objects function
;checks if item are in the current room and on you
(define (display-objects db id)
  (when (hash-has-key? db id)
    ;;join string if multiple items
    (let* ((record (hash-ref db id))
           (output (string-join record " and ")))
      (when (not (equal? output ""))
        (if (eq? id 'bag)
            (printf "You are carrying ~a.\n" output)
            (printf "You can see ~a.\n" output))))))


;display locations functions
;checks what location you are at
(define (display-locations db id)
  (when (hash-has-key? db id)
    ;;join string if multiple items
    (let* ((record (hash-ref db id))
           (output (string-join record " and ")))
      (when (not (equal? output ""))
        (if (eq? id 'bag)
            (printf "You are carrying ~a.\n" output)
            (printf "You can see ~a.\n" output))))))


;picking up item by removing it from room
;done by checking if hash exists, if so then check the reference
(define (remove-object-from-room db id str)
  (when (hash-has-key? db id)
    (let* ((record (hash-ref db id))
           ;result if the list of items left in the room
           (result (remove (lambda (x) (string-suffix-ci? str x)) record))
           ;item is the difference with the previous list e.g. the item collected
           (item (lset-difference equal? record result)))
      (cond ((null? item) 
             (printf "I don't see that item in the room!\n"))
            (else
             (printf "Added ~a to your coin bag.\n" (first item))
             (add-object inventorydb 'bag (first item))
             (hash-set! db id result))))))



;removing item fomr inventory
;done by checking if hash exists, if so then check the reference
(define (remove-object-from-inventory db id str)
  (when (hash-has-key? db 'bag)
    (let* ((record (hash-ref db 'bag))
           (result (remove (lambda (x) (string-suffix-ci? str x)) record))
           (item (lset-difference equal? record result)))
      (cond ((null? item)
             (printf "You are not carrying that item!\n"))
            (else
             (printf "Removed ~a from your coin bag.\n" (first item))
             (add-object objectdb id (first item))
             (hash-set! db 'bag result))))))


;function that picks up item
;calls the remove object from room function
(define (pick-item id input)
  (let ((item (string-join (cdr (string-split input)))))
    (remove-object-from-room objectdb id item)))


;function that puts item in inventory
;calls the remove object from inventory function
(define (put-item id input)
  (let ((item (string-join (cdr (string-split input)))))
    (remove-object-from-inventory inventorydb id item)))


;displaying the inventory function
(define (display-inventory)
  (display-objects inventorydb 'bag))



(define (slist->string l)
  (string-join (map symbol->string l)))

;function which gets the direction of the end of the game
(define (get-directions id)
  (let ((record (assq id decisiontable)))
    (let* ((result (filter (lambda (n) (number? (second n))) (cdr record)))
           (n (length result)))
      (cond ((= 0 n)
             (printf "You appear to have entered a room with no exits.\n"))
            ((= 1 n)
             (printf "You can see an exit to the ~a.\n" (slist->string (caar result))))
            (else
             (let* ((losym (map (lambda (x) (car x)) result))
                    (lostr (map (lambda (x) (slist->string x)) losym)))
               (printf "You can see exits to the ~a.\n" (string-join lostr " and "))))))))

(define (assq-ref assqlist id)
  (cdr (assq id assqlist)))

(define (assv-ref assqlist id)
  (cdr (assv id assqlist)))

(define (get-description id)
  (car (assq-ref descriptions id)))

(define (get-keywords id)
  (let ((keys (assq-ref decisiontable id)))
    (map (lambda (key) (car key)) keys)))


;; outputs a list in the form: (0 0 0 2 0 0)
(define (list-of-lengths keylist tokens)
  (map 
   (lambda (x)
     (let ((set (lset-intersection eq? tokens x)))
       ;; apply some weighting to the result
       (* (/ (length set) (length x)) (length set))))
   keylist))

(define (index-of-largest-number list-of-numbers)
  (let ((n (car (sort list-of-numbers >))))
    (if (zero? n)
      #f
      (list-index (lambda (x) (eq? x n)) list-of-numbers))))

;function which allows the user to check where the end is
(define (lookup id tokens)
  (let* ((record (assv-ref decisiontable id))
         (keylist (get-keywords id))
         (index (index-of-largest-number (list-of-lengths keylist tokens))))
    (if index 
      (cadr (list-ref record index))
      #f)))

(define (display-description id)
  (printf "~a\n" (get-description id)))

;function which starts the game
;use all the function created
(define (startgame initial-id)
  (let loop ((id initial-id) (description #t))
    (when description
      (display-description id)
      (display-objects objectdb id)
      (display-locations locationdb id))
    (printf "> ")
    (let* ((input (read-line))
           (string-tokens (string-tokenize input))
           (tokens (map string->symbol string-tokens)))
      (let ((response (lookup id tokens)))
        (cond ((number? response)
               (loop response #t))
              ((eq? #f response)
               (printf "huh? I didn't understand that!\n")
               (loop id #f))
              ((eq? response 'look)
               (get-directions id)
               (loop id #t))
              ((eq? response 'pick)
               (pick-item id input)
               (loop id #f))
              ((eq? response 'drop)
               (put-item id input)
               (loop id #f))
              ((eq? response 'inventory)
               (display-inventory)
               (loop id #f))              
              ((eq? response 'quit)
               (printf "Bye thanks for playing\n")
               (exit)))))))


(startgame 1)