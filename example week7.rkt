#lang racket

(require srfi/1)
(require srfi/13)
(require srfi/48)


(define (assq-ref assqlist id)
  (cdr (assq id assqlist)))

(define (assv-ref assqlist id)
  (cdr (assv id assqlist)))

(define (command)
  (let* ((input (read-line))
         (string-tokens (string-tokenize input))
         (tokens (map string->symbol string-tokens))
         (cmd (car tokens)))
    (cond
      ;; explain the commands
      ((eq? cmd `help)
       (format #t "Usage:\nsearch <term>\nquit\n"))
      ;; break the loop
      ((eq? cmd `quit)
       (exit))
      ;; do something
      ((eq? cmd `search)
       (format #t "Searching for ~a...\n" (cadr tokens)))
      ;; handle unknown input
      (else
       (format #t "Huh?\n"))))
  (command))

(define responses
  `((1 "What type of films do you like?")
    (2 "So you like gore?")
    (7 "Shall i recommend a gory film for you?")
    (8 "Shall i recommend a non-gory scary film for you?")
    (9 "What genre would you like yo watch")
    (10 "Here is a list of comedy films: A, B, C, D, E, F"))
  )

(define decisiontable
  `((1 ((comedy) 20) ((very scary) 2) ((thrillers) 6) ((not animated) 5) ((horror) 2) ((scfi) 4))
    (2 ((some) 8) ((a lot) 7) ((yes) 7) ((no) 8) ((not really) 8))
    (7 ((yes) gory) ((ok) gory) ((no) 9)))
    (9 ((comedy) 10) ((horror) 11)))

(define (get-response id)
  (car (assq-ref responses id)))

(define (get-keywords id)
  (let ((keys (assq-ref decisiontable id)))
    (map (λ (key) (car key)) keys)))

(define (list-of-lengths keylist tokens)
  (map
   (λ (x)
     (let ((set (lset-intersection eq? tokens x)))
       ;; apply some weighing to the result
       (* (/ (length set) (length x)) (length set))))
   keylist))

(define (index-of-largest-number list-of-numbers)
  (let ((n (car (sort list-of-numbers >))))
    (if (zero? n)
        #f
        (list-index (λ (x) (eq? x n)) list-of-numbers))))

(define (lookup id tokens)
  (let* ((record (assv-ref decisiontable id))
         (keylist (get-keywords id))
         (index (index-of-largest-number (list-of-lengths keylist tokens))))
    (if index
        (cadr (list-ref record index))
        #f)))

(define (recommend initial-id)
  (let loop ((id initial-id))
    (format #t "~a\n> " (get-response id))
    (let* ((input (read-line))
           (string-tokens (string-tokenize input))
           (tokens (map string->symbol string-tokens)))
      (let ((response (lookup id tokens)))
        (cond ((eq? #f response)
               (format #t "huh? i didn't understand that!" )
               (loop id))
              ((eq? `gory response)
               (format #t "Searching for gory horror films ....\n")
               (exit))
              ((eq? `non-gory response)
               (format #t "Searching for non-gory scary films ...\n")
               (exit))
              ((zero? response)
               (format #t "So long, and thanks for the fish...\n")
               (exit))
              (else
               (loop response)))))))